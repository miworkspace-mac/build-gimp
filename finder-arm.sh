#!/bin/bash

NEWLOC=`curl -L https://www.gimp.org/downloads/  2>/dev/null | /usr/local/bin/htmlq -a href a | grep arm64 | grep .dmg | tail -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo "https:"${NEWLOC}
fi